import { createApp } from 'vue';

// Pugins
import VueSmoothScroll from 'vue3-smooth-scroll';
import VueSvgInlinePlugin from 'vue-svg-inline-plugin';
import VueClipboard from 'vue3-clipboard';
import VueUniversalModal from 'vue-universal-modal';

import App from './App.vue';
import store from './store';

// Stylesheet
import './assets/scss/main.scss';
import 'vue-universal-modal/dist/index.css';

const app = createApp(App);
app.use(store);
app.use(VueSmoothScroll);
app.use(VueUniversalModal, {
  teleportTarget: '#modals',
});
app.use(VueSvgInlinePlugin);
app.use(VueClipboard);

app.mount('#app');
