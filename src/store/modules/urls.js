export default {
  state: () => ({
    external: {
      coffee: 'https://www.buymeacoffee.com/nikkitakin',
      feedback: 'https://github.com/nikkitasushkov/cottonicons/issues',
      github: 'https://github.com/nikkitasushkov/cottonicons/',
      figma: 'https://www.figma.com/community/file/1072882439751312255',
    },
    local: {
      download: 'https://github.com/nikkitasushkov/cottonicons/zipball/master/',
    },
  }),
};
