import { createStore } from 'vuex';
import icons from './modules/icons';
import urls from './modules/urls';

export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    icons,
    urls,
  },
});
